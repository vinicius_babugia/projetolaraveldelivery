<div class="form-group">
    {!! Form::label('Código', 'Código') !!}
    {!! Form::text('code', null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('Desconto R$', 'Desconto R$') !!}
    {!! Form::text('value', null, ['class'=>'form-control']) !!}
</div>