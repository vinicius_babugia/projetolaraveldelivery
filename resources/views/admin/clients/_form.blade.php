<div class="form-group">
    {!! Form::label('Name', 'Nome:') !!}
    {!! Form::text('user[name]', null, ['class'=>'form-control', 'placeholder'=>'Nome']) !!}
</div>
<div class="form-group">
    {!! Form::label('Email', 'Email:') !!}
    {!! Form::text('user[email]', null, ['class'=>'form-control', 'placeholder'=>'Email']) !!}
</div>
<div class="form-group">
    {!! Form::label('Telefone', 'Telefone:') !!}
    {!! Form::text('phone', null, ['class'=>'form-control', 'placeholder'=>'Telefone']) !!}
</div>
<div class="form-group">
    {!! Form::label('Endereço', 'Endereço:') !!}
    {!! Form::textarea('adress', null, ['class'=>'form-control', 'placeholder'=>'Endereço']) !!}
</div>
<div class="form-group">
    {!! Form::label('Cidade', 'Cidade:') !!}
    {!! Form::text('city', null, ['class'=>'form-control', 'placeholder'=>'Cidade']) !!}
</div>
<div class="form-group">
    {!! Form::label('Estado', 'Estado:') !!}
    {!! Form::text('state', null, ['class'=>'form-control', 'placeholder'=>'Estado']) !!}
</div>
<div class="form-group">
    {!! Form::label('Cep', 'Cep:') !!}
    {!! Form::text('zipcode', null, ['class'=>'form-control', 'placeholder'=>'Cep']) !!}
</div>