<!-- Extende do template app.blade.php -->
@extends('app')
<!-- Dentro do yield('contend') incrementamos nosso conteúdo pelo section -->
@section('content')
    <div class="container">
        <h3>Editando Cliente - {{$client->user->name}}</h3>
        @include('errors._check')
        {!! Form::model($client, ['route'=>['admin.clients.update', $client->id]]) !!}
            @include('admin.clients._form')
            <div class="form-group">
                {!! Form::submit('Salvar cliente', ['class'=>'btn btn-primary']) !!}
            </div>
        {!! Form::close() !!}
    </div>
@endsection