<div class="form-group">
    {!! Form::label('Category', 'Category:') !!}
    {!! Form::select('category_id', $categories, null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('Name', 'Nome:') !!}
    {!! Form::text('name', null, ['class'=>'form-control', 'placeholder'=>'Nome da categoria']) !!}
</div>
<div class="form-group">
    {!! Form::label('Description', 'Descrição:') !!}
    {!! Form::textarea('description', null, ['class'=>'form-control', 'placeholder'=>'Descrição']) !!}
</div>
<div class="form-group">
    {!! Form::label('Price', 'Preço:') !!}
    {!! Form::text('price', null, ['class'=>'form-control', 'placeholder'=>'Preço']) !!}
</div>
