<!-- Extende do template app.blade.php -->
@extends('app')
<!-- Dentro do yield('contend') incrementamos nosso conteúdo pelo section -->
@section('content')
    <div class="container">
        <h3>Novo Pedido</h3>
        @include('errors._check')
        <div class="container">
            {!! Form::open(['route'=>'customer.order.store', 'class'=>'form']) !!}
                <div class="form-group">
                    <label>Total:</label>
                    <p id="total"></p>
                    <a href="#" id="btnNewItem" class="btn btn-default">Novo Item</a>
                    <br/><br/>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Produto</th>
                                <th>Quantidade</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <select class="form-control" name="items[0][product_id]">
                                        @foreach($products as $p)
                                            <option value="{{$p->id}}" data-price="{{$p->price}}">{{$p->name}} --- {{$p->price}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    {!! Form::text('items[0][qtd]', 1, ['class'=>'form-control']) !!}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="form-group">
                    {!! Form::submit('Criar Pedido', ['class'=>'btn btn-primary']) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection

<!-- O java script que ser escrito aqui será utilizado no final do template app.blade.php -->
@section('post-script')
    <script>
        $('#btnNewItem').click(function() {
            // Pega a última linha do tbody
            var row = $('table tbody > tr:last'),
                // Clona a nossa linha passada, ou seja copia
                newRow = row.clone(),
                // Busca a quantidade de linhas
                length = $("table tbody tr").length;

            // Percorre a linha clonada
            newRow.find('td').each(function() {
                var td = $(this),
                    // Pega o input e o select
                    input = td.find('input,select'),
                    // Pega o name
                    name = input.attr('name');

                input.attr('name', name.replace((length - 1) + "", length + ""));
            });
            // Pega os dados do input e insere uma nova linha
            newRow.find('input').val(1);
            newRow.insertAfter(row);
            calculateTotal();
        });

        // Recalcula preço ao selecionar produto
        $(document.body).on('click', 'select', function(){
            calculateTotal();
        });

        // Toda a vez que a quantidade for alterada, recalcula
        $(document.body).on('blur', 'input', function(){
            calculateTotal();
        });

        function calculateTotal() {
            var total = 0,
                // Pega o tamanho da tr
                trlen = $('table tbody tr').length,
                // Seta o valor da tr
                tr = null, price, qtd;

            // Percorre as trs
            for (var i=0; i<trlen; i++) {
                tr = $('table tbody tr').eq(i);
                price = tr.find(':selected').data('price');
                qtd = tr.find('input').val();
                total += price * qtd;
            }
            $("#total").html(total);
        }

    </script>
@endsection