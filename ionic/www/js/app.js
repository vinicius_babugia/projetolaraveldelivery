// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'

// Criando módulo do angular e logo abaixo ele é identificado
angular.module('starter.controllers',[]);
// Criando módulo para abrigar os services e factory
angular.module('starter.services',[]);
// Criando módulo para abrigar os filtros
angular.module('starter.filters',[]);

angular.module('starter', [
    'ionic','starter.controllers','starter.services','starter.filters','angular-oauth2','ngResource', 'ngCordova'
])
  .constant('appConfig',{
      baseUrl: 'http://10.1.1.29:8100/projeto-curso/public'
  })
  .run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      if(window.cordova && window.cordova.plugins.Keyboard) {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

        // Don't remove this line unless you know what you are doing. It stops the viewport
        // from snapping when text inputs are focused. Ionic handles this internally for
        // a much nicer keyboard experience.
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if(window.StatusBar) {
        StatusBar.styleDefault();
      }
    });
  })

  .config(function($stateProvider,$urlRouterProvider,OAuthProvider,OAuthTokenProvider,appConfig,$provide){
    // Gera um serviço de OAuth, OAuth permite que faz acesso ao token, gera refresh token, realiza validações
    OAuthProvider.configure({
      baseUrl: appConfig.baseUrl,
      // Dados vindo da tabela oauth_clients
      clientId: 'appid01',
      clientSecret: 'secret', // optional
      grantPath: '/oauth/access_token'
    });

    // Tornando o Token seguro, gera o serviço do OAuthProvider, armazenando os dados de cookies e etc ..
    OAuthTokenProvider.configure({
       // Nome do cookie criado
       name: 'token',
       // Configurações do token
       options: {
          // Gera encriptação HTTPS se for true, como não usamos HTTPS fica como FALSE
          secure: false
       }
    });

    // $stateProvider Provedor de serviços do ui-router responsável por montar as rotas que a gente precisa
    $stateProvider
        .state('login',{
            url: '/login',
            templateUrl: 'templates/login.html',
            controller: 'LoginCtrl'
        })
        .state('home',{
            url: '/home',
            templateUrl: 'templates/home.html',
            controller: function($scope){
            }
        })
        // Rota ou estado abstrata onde possui o prefixo do client e somente ele poderá ter acesso, evitando do entregador acessar
        // Não poderá ser acessada, servirá como base para as outras rotas
        .state('client',{
            // Tenho uma rota base que não poderá ser reenderizada, ou seja somente rotas filhas serão chamada na página
            abstract: true,
            // Prefixo da rota ou estado /client, base para as outras rotas
            url: '/client',
            // A cada subrota que é chamada do client, por exemplo client/checkout, dentro do checkout ele já carrega o menu
            templateUrl: 'templates/client/menu.html',
            controller: 'ClientMenuCtrl'
        })
        // Listagem de pedidos
        .state('client.order', {
            cache: false,
            url: '/order',
            templateUrl: 'templates/client/order.html',
            controller: 'ClientOrderCtrl'
        })
        // Detalhes dos pedidos
        .state('client.view_order', {
            cache: false,
            url: '/view_order/:id',
            templateUrl: 'templates/client/view_order.html',
            controller: 'ClientViewOrderCtrl'
        })
        // Lista pedidos
        .state('client.checkout', {
            cache: false,
            url: '/checkout',
            templateUrl: 'templates/client/checkout.html',
            controller: 'ClientCheckoutCtrl'
        })
        // Detalhe de um item de checkout, ou detalhes do produtos e altera a quantidade
        .state('client.checkout_item_detail', {
            url: '/checkout/detail/:index',
            templateUrl: 'templates/client/checkout_item_detail.html',
            controller: 'ClientCheckoutDetailCtrl'
        })
        .state('client.checkout_successful',{
            cache: false,
            url: '/checkout/successful',
            templateUrl: 'templates/client/checkout_successful.html',
            controller: 'ClientCheckoutSuccessfulCtrl'
        })
        // Listagem de produtos disponíveis
        .state('client.view_products', {
            cache: false,
            url: '/view_products',
            templateUrl: 'templates/client/view_products.html',
            controller: 'ClientViewProductCtrl'
        })
        // Área do entregador, estado PAI
        .state('deliveryman',{
            abstract: true,
            url: '/deliveryman',
            templateUrl: 'templates/deliveryman/menu.html',
            controller: 'DeliverymanMenuCtrl'
        })
        // Pedidos do entregador
        .state('deliveryman.order', {
            abstract: true,
            url: '/order',
            templateUrl: 'templates/deliveryman/order.html',
            controller: 'DeliverymanOrderCtrl'
        })
        // Detalhes do Pedidos do entregador
        .state('deliveryman.view_order', {
            cache: false,
            url: '/view_order/:id',
            templateUrl: 'templates/deliveryman/view_order.html',
            controller: 'DeliverymanViewOrderCtrl'
        })
    // Sempre redireciona para a página principal se não achar a rota
    $urlRouterProvider.otherwise('/login');

    // Sobreescrevendo serviço o OAuthToken
    $provide.decorator('OAuthToken',['$localStorage','$delegate',function($localStorage,$delegate){
        // Definindo propriedades de um objeto
        Object.defineProperties($delegate, {
            setToken: {
                // Definindo um valor da propriedade que no caso é um método
                value: function(data) {
                    return $localStorage.setObject('token',data);
                },
                // Configurações do método
                enumerable: true, // Quando pegar o objeto é possível enxergar o método
                configurable: true, // Podemos mudar as propriedade do objeto
                writable: true // Se quisermos fazer um setToken = function() é possível
            },
            getToken: {
                value: function() {
                    return $localStorage.getObject('token');
                },
                enumerable: true,
                configurable: true,
                writable: true
            },
            removeToken: {
                value: function() {
                    return $localStorage.setObject('token', null);
                },
                enumerable: true,
                configurable: true,
                writable: true
            }
        });
        return $delegate;
    }]);

})
.service('cart',function(){
    this.items = [];
});

