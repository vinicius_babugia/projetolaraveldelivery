angular.module('starter.services')
    .factory('$localStorage',['$window', function($window){
        return {
            // Atribui dados puros ou primitivos e outros, atribuindo valor a chave e atributo
            // Não será utilizado para objeto, somente dados puro
            set: function(key,value) {
                // Chama o objeto window e aloca um espaço na key e inclui o valor
                $window.localStorage[key] = value;
                return $window.localStorage[key];
            },
            // Buscando os dados pela chave, default torna o key não obrigatório, retornando valor default
            get: function(key, defaultValue) {
                // Retorna os dados da chave key, se não existir a key, retorna o valor default
                return $window.localStorage[key] || defaultValue;
            },
            setObject: function(key,value) {
                // Precisamos transforma em um JSON para salvar o objeto por meio do JSON.stringify
                $window.localStorage[key] = JSON.stringify(value);
                return this.getObject(key);
            },
            getObject: function(key) {
                // Transformando o JSON em um objeto pelo JSON.parse se não existir passa NULL
                return JSON.parse($window.localStorage[key] || null);
            }
        };
    }]);