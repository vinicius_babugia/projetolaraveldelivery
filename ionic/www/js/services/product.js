angular.module('starter.services')
.factory('Product',['$resource','appConfig', function($resource, appConfig) {
    // 2 parametro - Parâmetros URL, tipo id=1
    // 3 parâmetro - Posso adicionar URLS, Mudar comportamentos e etc ...
    return $resource(appConfig.baseUrl+'/api/client/products',{}, {
        // Mudando comportamento do query
        query: {
            // Passa a aceitar array
            isArray: false
        }
    });
}]);