angular.module('starter.services')
    .factory('Order',['$resource','appConfig', function($resource, appConfig) {
        // 2 parametro - Parâmetros URL, tipo id=1
        // 3 parâmetro - Posso adicionar URLS, Mudar comportamentos e etc ...
        return $resource(appConfig.baseUrl+'/api/client/order/:id',{id: '@id'}, {
            // Mudando comportamento do query
            query: {
                // Passa a aceitar array
                isArray: false
            }
        });
    }]);