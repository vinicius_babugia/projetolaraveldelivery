angular.module('starter.services')
.factory('User',['$resource','appConfig', function($resource, appConfig) {
    // 2 parametro - Parâmetros URL, tipo id=1
    // 3 parâmetro - Posso adicionar URLS, Mudar comportamentos e etc ...
    return $resource(appConfig.baseUrl+'/api/authenticated',{}, {
        // Mudando comportamento do query
        query: {
            // Passa a aceitar array
            isArray: false
        },
        // Criando método de authenticated de forma isolada
        authenticated: {
            method: 'GET',
            url: appConfig.baseUrl+'/api/authenticated'
        }
    });
}]);