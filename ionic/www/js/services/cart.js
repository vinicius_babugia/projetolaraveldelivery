angular.module('starter.services')
    .service('$cart', ['$localStorage', function ($localStorage) {
        var key = 'cart', cartAux = $localStorage.getObject(key);

        if (!cartAux) {
            initCart();
        }

        // Função que limpa o carrinho
        this.clear = function() {
            initCart();
        };

        // Função que pega os dados do carrinho
        this.get = function() {
            return $localStorage.getObject(key);
        };

        // Função que pega um item do carrinho
        this.getItem = function(i) {
            return this.get().items[i];
        };

        // Função que adiciona o item
        this.addItem = function (item) {
            // Pega os itens do carrinho pelo método get acima e verifica se já existe produto inserido nele
            var cart = this.get(), itemAux, exists = false;
            for(var index in cart.items) {
                itemAux = cart.items[index];
                // Se já existir produto, atualiza quantidade e subtotal e para a execução com o break
                if(itemAux.id == item.id) {
                    itemAux.qtd = item.qtd + itemAux.qtd;
                    itemAux.subtotal = calculateSubTotal(itemAux);
                    exists = true;
                    break;
                }
            }
            // Se não existe produto no carrinho, adiciona nos items da variável cart criada logo acima
            if (!exists) {
                item.subtotal = calculateSubTotal(item);
                cart.items.push(item);
            }
            cart.total = getTotal(cart.items);
            // Adicionando cart ao local storage, pois sempre que alterar precisa modificar no local storage
            $localStorage.setObject(key, cart);
        };

        // Função que remove o item pelo índice do item
        this.removeItem = function (i) {
            var cart = $this.get();
            // Splice remove o parâmetro íncluído e quantos deseja remover no caso 1
            cart.items.splice(i,1)
            // Atualiza valor total do carrinho após o item ser removido
            cart.total = getTotal(cart.items);
            // Atualiza local storage novamente
            $localStorage.setObject(key,cart);
        };

        // Função que atualiza a quantidade pegando o índice do carrinho e a quantidade
        this.updateQtd = function(i, qtd) {
            var cart = this.get(),
                itemAux = cart.items[i];
            itemAux.qtd = qtd;
            itemAux.subtotal = calculateSubTotal(itemAux);
            cart.total = getTotal(cart.items);
            $localStorage.setObject(key,cart);
        };

        // Função que seta os dados do cupom no carrinho
        this.setCupom = function(code, value) {
            var cart = this.get();
            cart.cupom = {
                code: code,
                value: value
            };
            $localStorage.setObject(key,cart);
        };

        // Função que zera os dados do cupom no carrinho
        this.removeCupom = function() {
            var cart = this.get();
            cart.cupom = {
                code: null,
                value: null
            };
            $localStorage.setObject(key,cart);
        }
        
        this.getTotalFinal = function () {
            var cart = this.get();
            // (cart.cupom.value || 0) Diminui por um valor se existir se não retorna 0
            return cart.total - (cart.cupom.value || 0);
        }

        // Calcula o subtotal, quando se usa function é um método privado, não visto fora do serviço
        function calculateSubTotal(item){
            return item.price * item.qtd;
        };

        function getTotal(items) {
            var sum=0;
            // Varre os items e para cada item recebe uma função com o item, resgata o valor e atualiza total
            angular.forEach(items, function (item) {
                sum += item.subtotal;
            });
            return sum;
        };

        function initCart() {
            $localStorage.setObject(key,{
                items: [],
                total: 0,
                cupom: {
                    code: null,
                    value: null
                }
            });
        };

    }])
