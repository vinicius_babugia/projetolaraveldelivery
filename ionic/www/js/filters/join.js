angular.module('starter.filters')
    .filter('join',function(){
        // input pode ser dados,como array, texto de parâmetro
        // joinStr é o que vai ser juntado em uma string
        return function(input, joinStr){
            // .join pega o array e transforma em uma string separado pelo joinStr
            return input.join(joinStr);
        };
    });