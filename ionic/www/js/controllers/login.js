// Acessando módulo declarado no app.js
angular.module('starter.controllers')
    .controller('LoginCtrl',['$scope', 'OAuth', 'OAuthToken', '$ionicPopup', '$state', '$localStorage', 'User',
        function($scope, OAuth, OAuthToken, $ionicPopup, $state, $localStorage, User) {
        // Garante que os campos estão limpos
        $scope.user = {
           username: '',
           password: ''
        };

        $scope.login = function () {
            var promise = OAuth.getAccessToken($scope.user);
            promise
                // Primeiro executa a promessa de pegar acess token
                .then(function(data) {
                    return User.authenticated({include: 'client'}).$promise;
                })
                // Depois que pegou p acess token executa o sucesso da segunda promessa
                .then(function(data) {
                    $localStorage.set('user', data.data);
                    $state.go('client.checkout');
                // Se der erro em uma das 2 entra aqui
                }, function(responseError) {
                    // Limpa local storage e remove token
                    $localStorage.set('user',null);
                    OAuthToken.removeToken();
                    $ionicPopup.alert({
                        title: 'Advertência',
                        template: 'Login ou senha inválidos'
                    });
                });
        }

    }]);
