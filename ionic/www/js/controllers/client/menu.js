// Acessando módulo declarado no app.js
angular.module('starter.controllers')
    .controller('ClientMenuCtrl',[
        '$scope', '$state', '$ionicLoading', '$cart', 'User',
        function($scope, $state, $ionicLoading, $cart, User) {

            $scope.user = {
                name: ''
            };

            $ionicLoading.show({
                template: 'Carregando....'
            });

            User.authenticated({}, function(data){
                $scope.user = data.data;
                $ionicLoading.hide();
            },function(dataError){
                $ionicLoading.hide();
            });

        }]);
