// Acessando módulo declarado no app.js
angular.module('starter.controllers')
    .controller('ClientCheckoutDetailCtrl',[
        '$scope', '$state', '$stateParams', '$cart',function($scope, $state, $stateParams, $cart) {
        // Setando para a variável product os dados do carrinho que já tem os dados do produt
        // $stateParams.index pega o parâmetro index
        $scope.product = $cart.getItem($stateParams.index);

       // Atualizando a quantidade
       $scope.updateQtd = function() {
           $cart.updateQtd($stateParams.index, $scope.product.qtd);
           $state.go('client.checkout');
       }

    }]);
