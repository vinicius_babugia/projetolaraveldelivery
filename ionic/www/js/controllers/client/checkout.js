// Acessando módulo declarado no app.js
angular.module('starter.controllers')
    .controller('ClientCheckoutCtrl',[
        '$scope', '$state', '$cart', 'Order', '$ionicLoading', '$ionicPopup', 'Cupom', '$cordovaBarcodeScanner', 'User',
        function($scope, $state, $cart, Order, $ionicLoading, $ionicPopup, Cupom, $cordovaBarcodeScanner, User) {
            // Chamando método authenticated do service user, passando por parâmetro o client
            User.authenticated({include: 'client'},function (data) {
                console.log(data.data);
            }, function(responseError) {
                console.log(responseError);
            });

            var cart = $cart.get();
            $scope.cupom = cart.cupom;
            $scope.items = cart.items;
            $scope.total = $cart.getTotalFinal();

            $scope.removeItem = function(i) {
                $cart.removeItem(i);
                $scope.items.splice(i, 1);
                $scope.total = $cart.getTotalFinal();
            };

            $scope.openListProducts = function () {
                $state.go('client.view_products');
            };

            $scope.openProductDetail = function (i) {
                $state.go('client.checkout_item_detail',{index: i});
            };

            $scope.save = function() {
                // Criando uma cópia do objeto, ficando livre para poder modificá-lo
                var o = {items: angular.copy($scope.items)};
                // Percorre os items e cria um item a cada passada
                angular.forEach(o.items, function(item){
                    item.product_id = item.id;
                });
                $ionicLoading.show({
                    template: 'Salvando....'
                });

                if($scope.cupom.value){
                    o.cupom_code = $scope.cupom.code;
                }

                Order.save({id:null}, o,function(){
                    $ionicLoading.hide();
                    $state.go('client.checkout_successful');
                },function(responseError){
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Advertência',
                        template: 'Pedido não realizado, tente novamente!'
                    });
                });
            };

            // Lendo o cupom por meio do código de barras
            $scope.readBarCode = function() {
                // Invoca o serviço
                $cordovaBarcodeScanner
                // Efetuando a leitura
                .scan()
                .then(function(barcodeData) {
                    // Se a função foi de sucesso, barcodeData.text pega o texto do código de barras, existe outras funções
                    getValueCupom(barcodeData.text);
                }, function(error) {
                    $ionicPopup.alert({
                        title: 'Advertência',
                        template: 'Não foi possível ler o código de barras - Tente novamente'
                    });
                });
                //getValueCupom(4045);
            };

            // Remove o cupom
            $scope.removeCupom = function() {
                $cart.removeCupom();
                $scope.cupom = $cart.get().cupom;
                $scope.total = $cart.getTotalFinal();
            };

            // Atribui código do cupom e value, se não informa que o cupom é inválido
            function getValueCupom(code){
                $ionicLoading.show({
                    template: 'Carregando....'
                });
                // Efetuando consulta do cupom na API rest passando o código como parâmetro
                Cupom.get({code: code},function(data) {
                    $cart.setCupom(data.data.code,data.data.value);
                    $scope.cupom = $cart.get().cupom;
                    $scope.total = $cart.getTotalFinal();
                    $ionicLoading.hide();
                },function(responseError) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'Advertência',
                        template: 'Cupom inválido!'
                    });
                });
            }

    }]);
