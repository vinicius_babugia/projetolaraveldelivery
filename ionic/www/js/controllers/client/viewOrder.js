// Acessando módulo declarado no app.js
angular.module('starter.controllers')
    .controller('ClientViewOrderCtrl',[
        '$scope', '$stateParams', 'Order', '$ionicLoading',
        function($scope, $stateParams, Order, $ionicLoading) {
            // Iniciando um objeto vazio
            $scope.order = {};
            // Exibe um carregando na página até carregar os itens
            $ionicLoading.show({
                template: 'Carregando....'
            });

            // Buscando um objeto, por isso usa-se get e não query
            Order.get({id: $stateParams.id, include: "items, cupom"}, function(data) {
                $scope.order = data.data;
                $ionicLoading.hide();
            }, function(dataError) {
                $ionicLoading.hide();
            });

        }]);
