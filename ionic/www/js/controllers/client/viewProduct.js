// Acessando módulo declarado no app.js
angular.module('starter.controllers')
    .controller('ClientViewProductCtrl',[
        '$scope', '$state', 'Product', '$ionicLoading', '$cart',
        function($scope, $state, Product, $ionicLoading, $cart) {
            // Criando coleção de products
            $scope.products = [];
            // Exibe um carregando na página até carregar os itens
            $ionicLoading.show({
                template: 'Carregando....'
            });
            // Função de sucesso
            Product.query({}, function(data){
                $scope.products = data.data;
                // Escodendo Loading ao carregar a página
                $ionicLoading.hide();
            // Função de fracasso
            },function(dataError){
                console.log(dataError);
                $ionicLoading.hide();
            });
            
            $scope.addItem = function (item) {
                item.qtd = 1;
                $cart.addItem(item);
                $state.go('client.checkout');
            }
            
        }]);
