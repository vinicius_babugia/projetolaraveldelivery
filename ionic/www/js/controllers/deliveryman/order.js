// Acessando módulo declarado no app.js
angular.module('starter.controllers')
    .controller('DeliverymanOrderCtrl',[
        '$scope', '$state', '$ionicLoading', 'Order',
        function($scope, $state, $ionicLoading, Order) {

            $scope.items = [];

            $ionicLoading.show({
                template: 'Carregando....'
            });

            // Função de atualização de página do refresh
            $scope.doRefresh = function(){
                getOrders().then(function(data){
                    $scope.items = data.data;
                    // $broadcast sinal de chamada de evento, removendo o evento de refresh
                    $scope.$broadcast('scroll.refreshComplete');
                },function(dataError){
                    $scope.$broadcast('scroll.refreshComplete');
                });
            }

            $scope.openOrderDetail = function(order) {
                $state.go("client.view_order", {id: order.id});
            }

            function getOrders() {
                // $promise permite que faça esse bloco em outro lugar é uma promessa de execução
                return Order.query({
                    id: null,
                    // Parâmetros passado para o criteria do laravel da biblioteca l5 repository, configurado no repository
                    orderBy: 'created_at',
                    sortedBy: 'desc'
                }).$promise;
            };

            getOrders().then(function(data){
                $scope.items = data.data;
                $ionicLoading.hide();
            },function(dataError){
                $ionicLoading.hide();
            });

        }]);
