<?php
/**
 * Created by PhpStorm.
 * User: Elaine
 * Date: 07/05/2016
 * Time: 11:01
 */

namespace CodeDelivery\Services;


use CodeDelivery\Models\Order;
use CodeDelivery\Repositories\CupomRepository;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Repositories\ProductRepository;
use Illuminate\Support\Facades\DB;

class OrderService
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var CupomRepository
     */
    private $cupomRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;

    public function __construct(
        OrderRepository $orderRepository,
        CupomRepository $cupomRepository,
        ProductRepository $productRepository
    )
    {
        $this->orderRepository = $orderRepository;
        $this->cupomRepository = $cupomRepository;
        $this->productRepository = $productRepository;
    }

    public function create(array $data)
    {
        \DB::beginTransaction();
        try {
            // Setando status como pendente
            $data['status'] = 0;

            // Se usuário mandar cupom, remove por segurança
            if (isset($data['cupom_id'])) {
                unset($data['cupom_id']);
            }

            // Verifica se existe cupom e pega o primeiro registro e salva
            if (isset($data['cupom_code'])) {
                $cupom = $this->cupomRepository->findByField('code', $data['cupom_code'])->first();
                $data['cupom_id'] = $cupom->id;
                $cupom->used = 1;
                $cupom->save();
                unset($data['cupom_code']);
            }

            $items = $data['items'];
            unset($data['items']);

            // Salvando ordem no banco, posteriormente salvaremos os items
            $order = $this->orderRepository->create($data);
            $total = 0;

            // Percorrendo os items do pedido
            foreach($items as $item)
            {
                // Buscando o preço de cada item do produto
                $item['price'] = $this->productRepository->find($item['product_id'])->price;
                // Adicionando item no pedido por meio da ordem de pedido
                $order->items()->create($item);
                // Alimentando total
                $total += $item['price'] * $item['qtd'];
            }

            // Atualizando total da ordem de pedido
            $order->total = $total;
            // Validando cupom de desconto
            if (isset($cupom)) {
                $order->total = $total - $cupom->value;
            }

            $order->save();
            \DB::commit();
            return $order;
        } catch (\Exception $e) {
            \DB::rollback();
            throw $e;
        }
    }

    public function updateStatus($id, $idDeliveryman, $status)
    {
        $order = $this->orderRepository->getByIdAndDeliveryman($id, $idDeliveryman);

        if ($order instanceof Order) {
            $order->status = $status;
            $order->save();
            return $order;
        }
        return false;
    }

}