<?php

namespace CodeDelivery\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Order extends Model implements Transformable
{
    use TransformableTrait;

    // Mass Assigment
    // Todas as vezes que criamos um registro, devemos exigir que passe esse campo no construtor
    protected $fillable = [
        'client_id',
        'user_deliveryman_id',
        'total',
        'status',
        'cupom_id'
    ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function cupom()
    {
        return $this->belongsTo(Cupom::class);
    }

    public function items()
    {
        // Um pedido pode ter vários itens
        return $this->hasMany(OrderItem::class);
    }

    public function deliveryman()
    {
        // Um pedido pode ter um entregador, um entregador é um usuário
        return $this->belongsTo(User::class, 'user_deliveryman_id', 'id');
    }

}
