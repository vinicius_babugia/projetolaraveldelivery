<?php

namespace CodeDelivery\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Product extends Model implements Transformable
{
    use TransformableTrait;

    // Mass Assigment
    // Todas as vezes que criamos um registro, devemos exigir que passe esse campo no construtor
    protected $fillable = [
        'category_id', 'name', 'description', 'price'
    ];

    // Criando relacionamento com a categoria
    public function category()
    {
        // belongsTo -> Um produto pode conter uma categoria
        return $this->belongsTo(Category::class);
    }

}
