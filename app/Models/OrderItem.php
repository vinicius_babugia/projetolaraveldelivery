<?php

namespace CodeDelivery\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class OrderItem extends Model implements Transformable
{
    use TransformableTrait;

    // Mass Assigment
    // Todas as vezes que criamos um registro, devemos exigir que passe esse campo no construtor
    protected $fillable = [
        'product_id',
        'order_id',
        'price',
        'qtd'
    ];

    public function product()
    {
        // Um pedido pode ter vários produtos
        return $this->belongsTo(Product::class);
    }

    public function order()
    {
        // Um item pode estar em um pedido
        return $this->belongsTo(Order::class);
    }

}
