<?php

namespace CodeDelivery\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Client extends Model implements Transformable
{
    use TransformableTrait;

    // Mass Assigment
    // Todas as vezes que criamos um registro, devemos exigir que passe esse campo no construtor
    protected $fillable = [
        'user_id',
        'phone',
        'adress',
        'state',
        'zipcode'
    ];

    public function user()
    {
        // Um client pode ser um usuário somente
        // Como um cliente possui um usuário e não tem client_id, temos que definir
        // Que um cliente pode ter um usuário
        // Ele pega o id do usuário e faz relação com o user_id
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
