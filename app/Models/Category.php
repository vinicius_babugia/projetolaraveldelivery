<?php

namespace CodeDelivery\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Category extends Model implements Transformable
{
    use TransformableTrait;

    // Mass Assigment
    // Todas as vezes que criamos um registro, devemos exigir que passe esse campo no construtor
    protected $fillable = [
        'name'
    ];

    // Efetuando relacionamento com produto
    public function products()
    {
        // Uma categoria pode estar em vários produtos
        return $this->hasMany(Product::class);
    }

}
