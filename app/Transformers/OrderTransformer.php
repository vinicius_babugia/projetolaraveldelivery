<?php

namespace CodeDelivery\Transformers;

use Illuminate\Database\Eloquent\Collection;
use League\Fractal\TransformerAbstract;
use CodeDelivery\Models\Order;

/**
 * Class OrderTransformer
 * @package namespace CodeDelivery\Transformers;
 */
class OrderTransformer extends TransformerAbstract
{
    // Responsável por serializar os métodos que forem especificados, após serializar o transformer
    // Perceba a relação do Include com o includeCupom é o elo de ligação entre eles
    //protected $defaultIncludes = ['cupom', 'items'];
    // Não vão ser serializados por padrão, somente por chamado de métodos, pode ser chamados via GET ?include=cupom,items
    // Chame via GET pois as vezes a aplicação pode ficar pesada por causa disso
    // localhost:9090/code-delivery/public/api/client/order/1?include=cupom,items
    protected $availableIncludes = ['cupom', 'items', 'client'];

    /**
     * Transform the \Order entity
     * @param \Order $model
     *
     * @return array
     */
    public function transform(Order $model)
    {
        return [
            'id' => (int) $model->id,
            'total' => (float) $model->total,
            'product_names' => $this->getArrayProductNames($model->items),
            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }

    protected  function getArrayProductNames(Collection $items)
    {
        $names = [];
        foreach ($items as $item) {
            $names[] = $item->product->name;
        }
        return $names;
    }

    public function includeClient(Order $model)
    {
        return $this->item($model->client, new ClientTransformer());
    }

    public function includeCupom(Order $model)
    {
        if (!$model->cupom) {
            return null;
        }
        // Serializa um item do cupom
        return $this->item($model->cupom, new CupomTransformer());
    }

    public function includeItems(Order $model)
    {
        return $this->collection($model->items, new OrderItemTransformer());
    }

}
