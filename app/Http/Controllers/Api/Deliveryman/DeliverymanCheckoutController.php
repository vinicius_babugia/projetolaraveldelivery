<?php

namespace CodeDelivery\Http\Controllers\Api\Deliveryman;

use CodeDelivery\Http\Controllers\Controller;
use CodeDelivery\Http\Requests;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Repositories\UserRepository;
use CodeDelivery\Services\OrderService;
use Illuminate\Http\Request;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class DeliverymanCheckoutController extends Controller
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var OrderService
     */
    private $service;

    // Definindo as relações que teremos em nossas consultas
    private $with = ['client', 'cupom', 'items'];

    public function __construct(
        OrderRepository $orderRepository,
        UserRepository $userRepository,
        OrderService $service
    ){
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
        $this->service = $service;
    }

    public function index()
    {
        $id = Authorizer::getResourceOwnerId();
        // Trazendo os pedidos por entregador
        // Efetuando uma consulta fora do escopo, recebendo o mesmo objeto $query, ou seja incluindo um where na consulta
        // ->with(['items']) Acrescenta relação de itens a nossa consulta
        // ->with(['items','relacao2']) se quiser passar mais de um é só passar mais um parâmetro
        $orders = $this->orderRepository
                        ->skipPresenter(false)
                        ->with($this->with)->scopeQuery(function($query) use($id) {
                            return $query->where('user_deliveryman_id','=',$id);
                        })->paginate();
        // Passa a coleção de objetos e o laravel serializa em JSON
        return $orders;
    }

    public function show($id)
    {
        $idDeliveryman = Authorizer::getResourceOwnerId();
        return $this->orderRepository
                    ->skipPresenter(false)
                    ->getByIdAndDeliveryman($id, $idDeliveryman);
    }

    public function updateStatus(Request $request, $id)
    {
        $idDeliveryman = Authorizer::getResourceOwnerId();
        $order = $this->service->updateStatus($id, $idDeliveryman, $request->get('status'));
        if ($order) {
            return $this->orderRepository->find($order->id);
        }
        abort(400,"Order não encontrada");
    }

}
