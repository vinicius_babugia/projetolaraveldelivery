<?php

namespace CodeDelivery\Http\Controllers\Api\Client;

use CodeDelivery\Http\Controllers\Controller;
use CodeDelivery\Http\Requests;
use CodeDelivery\Http\Requests\AdminClientRequest;
use CodeDelivery\Http\Requests\CheckoutRequest;
use CodeDelivery\Repositories\OrderRepository;
use CodeDelivery\Repositories\ProductRepository;
use CodeDelivery\Repositories\UserRepository;
use CodeDelivery\Services\OrderService;
use Illuminate\Http\Request;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class ClientCheckoutController extends Controller
{
    /**
     * @var OrderRepository
     */
    private $orderRepository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var OrderService
     */
    private $service;

    // Definindo as relações que teremos em nossas consultas
    private $with = ['client', 'cupom', 'items'];

    public function __construct(
        OrderRepository $orderRepository,
        UserRepository $userRepository,
        ProductRepository $productRepository,
        OrderService $service
    ){
        $this->orderRepository = $orderRepository;
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
        $this->service = $service;
    }

    public function index()
    {
        $id = Authorizer::getResourceOwnerId();
        $clientId = $this->userRepository->find($id)->client->id;
        // Efetuando uma consulta fora do escopo, recebendo o mesmo objeto $query, ou seja incluindo um where na consulta
        // ->with(['items']) Acrescenta relação de itens a nossa consulta
        // ->with(['items','relacao2']) se quiser passar mais de um é só passar mais um parâmetro
        // ->skipPresenter(false) habilita o presenter para retornar os dados do transformer
        $orders = $this->orderRepository
            ->skipPresenter(false)
            ->with($this->with)
            ->scopeQuery(function($query) use($clientId) {
            return $query->where('client_id','=',$clientId);
        })->paginate();
        // Passa a coleção de objetos e o laravel serializa em JSON
        return $orders;
    }

    public function store(CheckoutRequest $request)
    {
        $data = $request->all();
        // Peguei o usuário, com o usuário, peguei o cliente, porque é tudo amarrado com o client_id
        $id = Authorizer::getResourceOwnerId();
        $clientId = $this->userRepository->find($id)->client->id;
        $data['client_id'] = $clientId;
        $order = $this->service->create($data);
        //$orderItems = $this->orderRepository->with('items')->find($order->id);
        return $this->orderRepository
                    ->skipPresenter(false)
                    ->with($this->with)
                    ->find($order->id);
    }

    public function show($id)
    {
        return $this->orderRepository
                    ->skipPresenter(false)
                    ->with($this->with)
                    ->find($id);
    }

}
