<?php

namespace CodeDelivery\Http\Middleware;

use Closure;
use CodeDelivery\Repositories\UserRepository;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class OAuthCheckRole
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Handle an incoming request.
     * Este método é executado quando o middleware é executado
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        // Pega id do usuário autenticado por meio da Facade
        $id = Authorizer::getResourceOwnerId();
        // Pega os dados do usuário
        $user = $this->userRepository->find($id);
        // Confere role
        if ($user->role != $role) {
            abort(403, 'Acess Forbidden');
        }

        return $next($request);
    }
}
