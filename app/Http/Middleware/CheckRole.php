<?php

namespace CodeDelivery\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     * Este método é executado quando o middleware é executado
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role) //aqui adicionamos um parametro para o middleware
    {
        if (!Auth::check()) {
            return redirect('/auth/login');
        }

        if (Auth::user()->role <> $role){ // Se a role do usuário autenticado não bate com a role especificada, login
            return redirect('/auth/login');
        }

        return $next($request);
    }
}
