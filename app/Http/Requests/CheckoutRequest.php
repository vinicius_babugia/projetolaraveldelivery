<?php

namespace CodeDelivery\Http\Requests;

// Renomeando por causa de conflito
use Illuminate\Http\Request as HttpRequest;

class CheckoutRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(HttpRequest $request)
    {
        // Validando cupom se ele for informado, existir no banco e não foi usado
        $rules = [
            "cupom_code" => 'exists:cupoms,code,used,0'
        ];
        // Método que valida os items
        $this->buildRulesItems(0, $rules);
        // Força a criar um array em branco se vier em branco ou não existir para validação funcionar
        $items = $request->get('items', []);
        $items = !is_array($items) ? [] : $items;
        // Percorrendo os items e validando
        foreach ($items as $key => $val) {
            $this->buildRulesItems($key, $rules);
        }

        return $rules;
    }

    public function buildRulesItems($key, array &$rules)
    {
        $rules["items.$key.product_id"] = 'required';
        $rules["items.$key.qtd"] = 'required';
    }

}
