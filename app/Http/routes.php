<?php

Route::get('/', function() {
    return view('welcome');
});

Route::get('/home', function() {
    return view('welcome');
});

Route::group(['prefix'=>'admin/', 'middleware' => 'auth.checkrole:admin', 'as' => 'admin.'], function(){
    /* --------------------------------------------------------------------------------------------------------------
    /   MIDDLEWARE
    /  'middleware' => 'auth.checkrole' fica dentro do Kernel.php, onde todas as vezes que executar o admin
    /   vai para o middleware CheckRole onde será realizada as validações de autenticação
    /---------------------------------------------------------------------------------------------------------------*/

    // Categories
    Route::get('categories',['as'=>'categories.index', 'uses'=>'CategoriesController@index']);
    Route::get('categories/create', ['as' => 'categories.create', 'uses' => 'CategoriesController@create']);
    Route::get('categories/edit/{id}', ['as' => 'categories.edit', 'uses' => 'CategoriesController@edit']);
    Route::post('categories/update/{id}', ['as' => 'categories.update', 'uses' => 'CategoriesController@update']);
    Route::post('categories/store', ['as' => 'categories.store', 'uses' => 'CategoriesController@store']);
    // Fim Categories

    // Products
    Route::get('products',['as'=>'products.index', 'uses'=>'ProductsController@index']);
    Route::get('products/create', ['as' => 'products.create', 'uses' => 'ProductsController@create']);
    Route::get('products/edit/{id}', ['as' => 'products.edit', 'uses' => 'ProductsController@edit']);
    Route::get('products/destroy/{id}', ['as' => 'products.destroy', 'uses' => 'ProductsController@destroy']);
    Route::post('products/update/{id}', ['as' => 'products.update', 'uses' => 'ProductsController@update']);
    Route::post('products/store', ['as' => 'products.store', 'uses' => 'ProductsController@store']);
    // Fim Products

    // Clients
    Route::get('clients',['as'=>'clients.index', 'uses'=>'ClientsController@index']);
    Route::get('clients/create', ['as' => 'clients.create', 'uses' => 'ClientsController@create']);
    Route::get('clients/edit/{id}', ['as' => 'clients.edit', 'uses' => 'ClientsController@edit']);
    Route::post('clients/update/{id}', ['as' => 'clients.update', 'uses' => 'ClientsController@update']);
    Route::post('clients/store', ['as' => 'clients.store', 'uses' => 'ClientsController@store']);
    // Fim Clients

    // Orders
    Route::get('orders',['as'=>'orders.index', 'uses'=>'OrdersController@index']);
    Route::get('orders/edit/{id}',['as'=>'orders.edit', 'uses'=>'OrdersController@edit']);
    Route::post('orders/update/{id}',['as'=>'orders.update', 'uses'=>'OrdersController@update']);
    // Fim Orders

    // Cupoms
    Route::get('cupoms',['as'=>'cupoms.index', 'uses'=>'CupomsController@index']);
    Route::get('cupoms/create',['as'=>'cupoms.create', 'uses'=>'CupomsController@create']);
    Route::post('cupoms/store',['as'=>'cupoms.store', 'uses'=>'CupomsController@store']);
    // Fim Cupoms
});

// Pedidos
Route::group(['prefix'=>'customer', 'middleware' => 'auth.checkrole:client', 'as' => 'customer.'], function() {

    Route::get('order', ['as'=>'order.index', 'uses'=>'CheckoutController@index']);
    Route::get('order/create', ['as'=>'order.create', 'uses'=>'CheckoutController@create']);
    Route::post('order/store', ['as'=>'order.store', 'uses'=>'CheckoutController@store']);

});

Route::group(['middleware' => 'cors'], function() {
    // Rota que gera o token de acesso as apis
    Route::post('oauth/access_token', function() {
        // Esse cara chama o config -> oauth2.php e devolve o token em JSON
        return Response::json(Authorizer::issueAccessToken());
    });

    // Criando rotas de agrupamento das API'S
    Route::group(['prefix'=>'api', 'middleware' => 'oauth', 'as' => 'api.'], function() {

        // Rota de acesso a api de clientes
        Route::group(['prefix'=>'client', 'middleware'=>'oauth.checkrole:client',  'as' => 'client.'], function() {
            // Cria os método HTTP que a gente precisa baseado no nosso controller
            // https://laravel.com/docs/5.1/controllers#restful-supplementing-resource-controllers
            Route::resource('order','Api\Client\ClientCheckoutController',['except'=>['create','edit','destroy']]);
            // Listagem de produtos
            Route::get('products','Api\Client\ClientProductController@index');
        });

        // Rota de acesso a api de deliveryman
        Route::group(['prefix'=>'deliveryman', 'middleware'=>'oauth.checkrole:deliveryman', 'as' => 'deliveryman.'], function() {
            // Cria os método HTTP que a gente precisa baseado no nosso controller
            // https://laravel.com/docs/5.1/controllers#restful-supplementing-resource-controllers
            Route::resource('order', 'Api\Deliveryman\DeliverymanCheckoutController',['except'=>['create','edit','destroy','store']]);

            // Rota de atualização do status
            // Patch, atualização de alguns dados de uma parte de um recurso
            Route::patch('order/{id}/update-status', [
                'uses' => 'Api\Deliveryman\DeliverymanCheckoutController@updateStatus',
                'as' => 'orders.update_status']);
        });

        Route::get('authenticated', 'Api\UserController@authenticated');
        Route::get('cupom/{code}', 'Api\CupomController@show');

    });
});


