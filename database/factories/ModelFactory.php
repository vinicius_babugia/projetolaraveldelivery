<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use CodeDelivery\Models\Category;
use CodeDelivery\Models\Client;
use CodeDelivery\Models\Cupom;
use CodeDelivery\Models\Order;
use CodeDelivery\Models\OrderItem;
use CodeDelivery\Models\Product;

$factory->define(CodeDelivery\Models\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

// Cria objeto para manipular os dados no BD
// Biblioteca Faker, gera dados fake de manipulação
$factory->define(Category::class, function(Faker\Generator $faker){
    // Retorna o nome da categoria
    return [
        'name' => $faker->word
    ];
});

// Cria objeto para manipular os dados no BD
// Biblioteca Faker, gera dados fake de manipulação
$factory->define(Product::class, function(Faker\Generator $faker){
    // Retorna o nome da categoria
    return [
        'name' => $faker->word,
        'description' => $faker->sentence,
        // Cria um número entre 10 e 50
        'price' => $faker->numberBetween(10, 50)
    ];
});

$factory->define(Order::class, function(Faker\Generator $faker){
    return [
        'client_id' => rand(1,10),
        'total' => rand(50, 100),
        'status' => 0,
    ];
});

$factory->define(OrderItem::class, function(Faker\Generator $faker){
    return [

    ];
});


// Cria objeto para manipular os dados no BD
// Biblioteca Faker, gera dados fake de manipulação
$factory->define(Client::class, function(Faker\Generator $faker){
    // Retorna o nome da categoria
    return [
        'phone' => $faker->phoneNumber,
        'adress' => $faker->address,
        'city' => $faker->city,
        'state' => $faker->state,
        'zipcode' => $faker->postcode
    ];
});

// Cria objeto para manipular os dados no BD
// Biblioteca Faker, gera dados fake de manipulação
$factory->define(Cupom::class, function(Faker\Generator $faker){
    return [
        'code' => rand(100, 10000),
        'value' => rand(50, 100)
    ];
});
