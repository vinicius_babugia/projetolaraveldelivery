<?php

use CodeDelivery\Models\Category;
use CodeDelivery\Models\Product;
use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Para cada category $c, crie os produtos
        factory(Category::class, 10)->create()->each(function($c) {
            for ($i=0; $i<=5; $i++) {
                // Cria o objeto na memória factory(\CodeDelivery\Models\Product::class)->make()
                // save -> Salva os produtos que aponta para o método products do Model Category
                $c->products()->save(factory(Product::class)->make());
            }
        });
    }
}
