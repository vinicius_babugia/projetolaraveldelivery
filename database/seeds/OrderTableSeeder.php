<?php

use CodeDelivery\Models\Order;
use CodeDelivery\Models\OrderItem;
use Illuminate\Database\Seeder;

class OrderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Para cada category $c, crie os produtos
        factory(Order::class, 10)->create()->each(function($o) {
            for ($i=0; $i<=2; $i++) {
                // Cria o objeto na memória factory(\CodeDelivery\Models\Product::class)->make()
                // save -> Salva os items do pedido que aponta para o método items do Model Order
                $o->items()->save(factory(OrderItem::class)->make([
                    'product_id' => rand(1, 5),
                    'price' => 50,
                    'qtd' => 2,
                ]));
            }
        });
    }
}
